//uScript Generated Code - Build 1.0.2522
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class saharamaterialchange : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   UnityEngine.Material local_100_UnityEngine_Material = null;
   UnityEngine.Material local_101_UnityEngine_Material = null;
   UnityEngine.GameObject local_108_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_108_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_109_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_109_UnityEngine_GameObject_previous = null;
   UnityEngine.Material local_116_UnityEngine_Material = null;
   UnityEngine.Material local_117_UnityEngine_Material = null;
   UnityEngine.Material local_118_UnityEngine_Material = null;
   UnityEngine.Material local_119_UnityEngine_Material = null;
   UnityEngine.Material local_120_UnityEngine_Material = null;
   UnityEngine.Material local_121_UnityEngine_Material = null;
   UnityEngine.GameObject local_14_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_14_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_15_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_15_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_16_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_16_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_17_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_17_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_18_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_18_UnityEngine_GameObject_previous = null;
   UnityEngine.Material local_22_UnityEngine_Material = null;
   UnityEngine.Material local_23_UnityEngine_Material = null;
   UnityEngine.Material local_24_UnityEngine_Material = null;
   UnityEngine.Material local_3_UnityEngine_Material = null;
   UnityEngine.Material local_36_UnityEngine_Material = null;
   UnityEngine.Material local_37_UnityEngine_Material = null;
   UnityEngine.Material local_38_UnityEngine_Material = null;
   UnityEngine.Material local_39_UnityEngine_Material = null;
   UnityEngine.Material local_4_UnityEngine_Material = null;
   UnityEngine.Material local_40_UnityEngine_Material = null;
   UnityEngine.Material local_56_UnityEngine_Material = null;
   UnityEngine.Material local_57_UnityEngine_Material = null;
   UnityEngine.Material local_58_UnityEngine_Material = null;
   UnityEngine.Material local_59_UnityEngine_Material = null;
   UnityEngine.Material local_6_UnityEngine_Material = null;
   UnityEngine.GameObject local_62_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_62_UnityEngine_GameObject_previous = null;
   UnityEngine.Material local_72_UnityEngine_Material = null;
   UnityEngine.Material local_73_UnityEngine_Material = null;
   UnityEngine.Material local_74_UnityEngine_Material = null;
   UnityEngine.GameObject local_77_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_77_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_8_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_8_UnityEngine_GameObject_previous = null;
   UnityEngine.Material local_85_UnityEngine_Material = null;
   UnityEngine.Material local_86_UnityEngine_Material = null;
   UnityEngine.Material local_87_UnityEngine_Material = null;
   UnityEngine.GameObject local_94_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_94_UnityEngine_GameObject_previous = null;
   UnityEngine.GameObject local_95_UnityEngine_GameObject = null;
   UnityEngine.GameObject local_95_UnityEngine_GameObject_previous = null;
   UnityEngine.Material local_99_UnityEngine_Material = null;
   public System.Single Mainstagetiming = (float) 2;
   public System.Single SaharaTiming = (float) 2;
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_1 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_1 = "s_1a";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_1;
   bool logic_uScriptAct_LoadMaterial_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_2 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_2 = "s_2a";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_2;
   bool logic_uScriptAct_LoadMaterial_Out_2 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_5 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_5 = "s_1b";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_5;
   bool logic_uScriptAct_LoadMaterial_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_7 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_7 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_7 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_7 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_7 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_9 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_9 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_9 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_9 = true;
   bool logic_uScriptAct_Delay_AfterDelay_9 = true;
   bool logic_uScriptAct_Delay_Stopped_9 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_9 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_10 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_10 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_10 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_10 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_10 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_11 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_11 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_11 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_11 = true;
   bool logic_uScriptAct_Delay_AfterDelay_11 = true;
   bool logic_uScriptAct_Delay_Stopped_11 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_11 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_12 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_12 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_12 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_12 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_12 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_13 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_13 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_13 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_13 = true;
   bool logic_uScriptAct_Delay_AfterDelay_13 = true;
   bool logic_uScriptAct_Delay_Stopped_13 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_13 = false;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_19 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_19 = "s_2b";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_19;
   bool logic_uScriptAct_LoadMaterial_Out_19 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_20 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_20 = "s_3a";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_20;
   bool logic_uScriptAct_LoadMaterial_Out_20 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_21 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_21 = "s_3b";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_21;
   bool logic_uScriptAct_LoadMaterial_Out_21 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_25 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_25 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_25 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_25 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_25 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_26 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_26 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_26 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_26 = true;
   bool logic_uScriptAct_Delay_AfterDelay_26 = true;
   bool logic_uScriptAct_Delay_Stopped_26 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_26 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_27 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_27 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_27 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_27 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_27 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_28 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_28 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_28 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_28 = true;
   bool logic_uScriptAct_Delay_AfterDelay_28 = true;
   bool logic_uScriptAct_Delay_Stopped_28 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_28 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_29 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_29 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_29 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_29 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_29 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_30 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_30 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_30 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_30 = true;
   bool logic_uScriptAct_Delay_AfterDelay_30 = true;
   bool logic_uScriptAct_Delay_Stopped_30 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_30 = false;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_31 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_31 = "s_3c";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_31;
   bool logic_uScriptAct_LoadMaterial_Out_31 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_32 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_32 = "s_3d";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_32;
   bool logic_uScriptAct_LoadMaterial_Out_32 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_33 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_33 = "s_3e calm";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_33;
   bool logic_uScriptAct_LoadMaterial_Out_33 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_34 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_34 = "s_4a";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_34;
   bool logic_uScriptAct_LoadMaterial_Out_34 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_35 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_35 = "s_4b";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_35;
   bool logic_uScriptAct_LoadMaterial_Out_35 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_41 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_41 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_41 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_41 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_41 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_42 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_42 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_42 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_42 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_42 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_43 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_43 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_43 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_43 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_43 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_44 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_44 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_44 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_44 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_44 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_45 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_45 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_45 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_45 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_45 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_46 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_46 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_46 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_46 = true;
   bool logic_uScriptAct_Delay_AfterDelay_46 = true;
   bool logic_uScriptAct_Delay_Stopped_46 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_46 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_47 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_47 = (float) 2;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_47 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_47 = true;
   bool logic_uScriptAct_Delay_AfterDelay_47 = true;
   bool logic_uScriptAct_Delay_Stopped_47 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_47 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_48 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_48 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_48 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_48 = true;
   bool logic_uScriptAct_Delay_AfterDelay_48 = true;
   bool logic_uScriptAct_Delay_Stopped_48 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_48 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_49 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_49 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_49 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_49 = true;
   bool logic_uScriptAct_Delay_AfterDelay_49 = true;
   bool logic_uScriptAct_Delay_Stopped_49 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_49 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_50 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_50 = (float) 0.7;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_50 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_50 = true;
   bool logic_uScriptAct_Delay_AfterDelay_50 = true;
   bool logic_uScriptAct_Delay_Stopped_50 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_50 = false;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_52 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_52 = "M_1";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_52;
   bool logic_uScriptAct_LoadMaterial_Out_52 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_53 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_53 = "M_2";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_53;
   bool logic_uScriptAct_LoadMaterial_Out_53 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_54 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_54 = "M_3";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_54;
   bool logic_uScriptAct_LoadMaterial_Out_54 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_55 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_55 = "M_4";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_55;
   bool logic_uScriptAct_LoadMaterial_Out_55 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_60 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_60 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_60 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_60 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_60 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_61 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_61 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_61 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_61 = true;
   bool logic_uScriptAct_Delay_AfterDelay_61 = true;
   bool logic_uScriptAct_Delay_Stopped_61 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_61 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_63 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_63 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_63 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_63 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_63 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_64 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_64 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_64 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_64 = true;
   bool logic_uScriptAct_Delay_AfterDelay_64 = true;
   bool logic_uScriptAct_Delay_Stopped_64 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_64 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_65 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_65 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_65 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_65 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_65 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_66 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_66 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_66 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_66 = true;
   bool logic_uScriptAct_Delay_AfterDelay_66 = true;
   bool logic_uScriptAct_Delay_Stopped_66 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_66 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_67 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_67 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_67 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_67 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_67 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_68 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_68 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_68 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_68 = true;
   bool logic_uScriptAct_Delay_AfterDelay_68 = true;
   bool logic_uScriptAct_Delay_Stopped_68 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_68 = false;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_69 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_69 = "Lightweaver 1";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_69;
   bool logic_uScriptAct_LoadMaterial_Out_69 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_70 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_70 = "Lightweaver 2";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_70;
   bool logic_uScriptAct_LoadMaterial_Out_70 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_71 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_71 = "Lightweaver";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_71;
   bool logic_uScriptAct_LoadMaterial_Out_71 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_75 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_75 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_75 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_75 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_75 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_76 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_76 = (float) 5;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_76 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_76 = true;
   bool logic_uScriptAct_Delay_AfterDelay_76 = true;
   bool logic_uScriptAct_Delay_Stopped_76 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_76 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_78 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_78 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_78 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_78 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_78 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_79 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_79 = (float) 5;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_79 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_79 = true;
   bool logic_uScriptAct_Delay_AfterDelay_79 = true;
   bool logic_uScriptAct_Delay_Stopped_79 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_79 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_80 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_80 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_80 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_80 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_80 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_81 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_81 = (float) 5;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_81 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_81 = true;
   bool logic_uScriptAct_Delay_AfterDelay_81 = true;
   bool logic_uScriptAct_Delay_Stopped_81 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_81 = false;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_82 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_82 = "Squaredtestexport_Squarelighting";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_82;
   bool logic_uScriptAct_LoadMaterial_Out_82 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_83 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_83 = "Cubetree2";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_83;
   bool logic_uScriptAct_LoadMaterial_Out_83 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_84 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_84 = "Cubetree3";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_84;
   bool logic_uScriptAct_LoadMaterial_Out_84 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_88 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_88 = (float) 10;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_88 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_88 = true;
   bool logic_uScriptAct_Delay_AfterDelay_88 = true;
   bool logic_uScriptAct_Delay_Stopped_88 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_88 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_89 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_89 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_89 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_89 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_89 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_90 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_90 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_90 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_90 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_90 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_91 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_91 = (float) 10;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_91 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_91 = true;
   bool logic_uScriptAct_Delay_AfterDelay_91 = true;
   bool logic_uScriptAct_Delay_Stopped_91 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_91 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_92 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_92 = (float) 10;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_92 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_92 = true;
   bool logic_uScriptAct_Delay_AfterDelay_92 = true;
   bool logic_uScriptAct_Delay_Stopped_92 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_92 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_93 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_93 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_93 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_93 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_93 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_96 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_96 = "Ferrislights1";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_96;
   bool logic_uScriptAct_LoadMaterial_Out_96 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_97 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_97 = "Ferrislights2";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_97;
   bool logic_uScriptAct_LoadMaterial_Out_97 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_98 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_98 = "Ferrislights3";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_98;
   bool logic_uScriptAct_LoadMaterial_Out_98 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_102 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_102 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_102 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_102 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_102 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_103 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_103 = (float) 10;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_103 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_103 = true;
   bool logic_uScriptAct_Delay_AfterDelay_103 = true;
   bool logic_uScriptAct_Delay_Stopped_103 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_103 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_104 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_104 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_104 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_104 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_104 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_105 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_105 = (float) 10;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_105 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_105 = true;
   bool logic_uScriptAct_Delay_AfterDelay_105 = true;
   bool logic_uScriptAct_Delay_Stopped_105 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_105 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_106 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_106 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_106 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_106 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_106 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_107 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_107 = (float) 10;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_107 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_107 = true;
   bool logic_uScriptAct_Delay_AfterDelay_107 = true;
   bool logic_uScriptAct_Delay_Stopped_107 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_107 = false;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_110 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_110 = "M_5";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_110;
   bool logic_uScriptAct_LoadMaterial_Out_110 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_111 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_111 = "M_6";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_111;
   bool logic_uScriptAct_LoadMaterial_Out_111 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_112 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_112 = "M_10";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_112;
   bool logic_uScriptAct_LoadMaterial_Out_112 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_113 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_113 = "M_7";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_113;
   bool logic_uScriptAct_LoadMaterial_Out_113 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_114 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_114 = "M_8";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_114;
   bool logic_uScriptAct_LoadMaterial_Out_114 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadMaterial logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_115 = new uScriptAct_LoadMaterial( );
   System.String logic_uScriptAct_LoadMaterial_name_115 = "M_9";
   UnityEngine.Material logic_uScriptAct_LoadMaterial_materialFile_115;
   bool logic_uScriptAct_LoadMaterial_Out_115 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_122 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_122 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_122 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_122 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_122 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_123 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_123 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_123 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_123 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_123 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_124 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_124 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_124 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_124 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_124 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_125 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_125 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_125 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_125 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_125 = true;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_126 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_126 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_126 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_126 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_126 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_127 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_127 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_127 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_127 = true;
   bool logic_uScriptAct_Delay_AfterDelay_127 = true;
   bool logic_uScriptAct_Delay_Stopped_127 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_127 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_128 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_128 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_128 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_128 = true;
   bool logic_uScriptAct_Delay_AfterDelay_128 = true;
   bool logic_uScriptAct_Delay_Stopped_128 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_128 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_129 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_129 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_129 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_129 = true;
   bool logic_uScriptAct_Delay_AfterDelay_129 = true;
   bool logic_uScriptAct_Delay_Stopped_129 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_129 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_130 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_130 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_130 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_130 = true;
   bool logic_uScriptAct_Delay_AfterDelay_130 = true;
   bool logic_uScriptAct_Delay_Stopped_130 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_130 = false;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_131 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_131 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_131 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_131 = true;
   bool logic_uScriptAct_Delay_AfterDelay_131 = true;
   bool logic_uScriptAct_Delay_Stopped_131 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_131 = false;
   //pointer to script instanced logic node
   uScriptAct_AssignMaterial logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_132 = new uScriptAct_AssignMaterial( );
   UnityEngine.GameObject[] logic_uScriptAct_AssignMaterial_Target_132 = new UnityEngine.GameObject[] {};
   UnityEngine.Material logic_uScriptAct_AssignMaterial_materialName_132 = null;
   System.Int32 logic_uScriptAct_AssignMaterial_MatChannel_132 = (int) 0;
   bool logic_uScriptAct_AssignMaterial_Out_132 = true;
   //pointer to script instanced logic node
   uScriptAct_Delay logic_uScriptAct_Delay_uScriptAct_Delay_133 = new uScriptAct_Delay( );
   System.Single logic_uScriptAct_Delay_Duration_133 = (float) 0;
   System.Boolean logic_uScriptAct_Delay_SingleFrame_133 = (bool) false;
   bool logic_uScriptAct_Delay_Immediate_133 = true;
   bool logic_uScriptAct_Delay_AfterDelay_133 = true;
   bool logic_uScriptAct_Delay_Stopped_133 = true;
   bool logic_uScriptAct_Delay_DrivenDelay_133 = false;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = null;
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_8_UnityEngine_GameObject = GameObject.Find( "saharastage3:SaharaMainlightingpanels" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_14_UnityEngine_GameObject = GameObject.Find( "saharastage3:OverheadDisplays" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_15_UnityEngine_GameObject = GameObject.Find( "saharastage3:Saharapanelcenter" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_16_UnityEngine_GameObject = GameObject.Find( "saharastage3:Saharapanelstageleft" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_17_UnityEngine_GameObject = GameObject.Find( "saharastage3:Saharapanelstageright" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_18_UnityEngine_GameObject = GameObject.Find( "saharastage3:Sidelight_displays" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_62_UnityEngine_GameObject = GameObject.Find( "mainstagescreens2" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_77_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_77_UnityEngine_GameObject = GameObject.Find( "lightweaverexport1" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_77_UnityEngine_GameObject_previous != local_77_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_77_UnityEngine_GameObject_previous = local_77_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_94_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_94_UnityEngine_GameObject = GameObject.Find( "Squaredtestexport_Cubes" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_94_UnityEngine_GameObject_previous != local_94_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_94_UnityEngine_GameObject_previous = local_94_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_95_UnityEngine_GameObject = GameObject.Find( "Squaredtestexport_Cubes 1" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_95_UnityEngine_GameObject_previous != local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_95_UnityEngine_GameObject_previous = local_95_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_108_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_108_UnityEngine_GameObject = GameObject.Find( "FerriswheelEfficent:FerrisWheelstars" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_108_UnityEngine_GameObject_previous != local_108_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_108_UnityEngine_GameObject_previous = local_108_UnityEngine_GameObject;
         
         //setup new listeners
      }
      if ( null == local_109_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         local_109_UnityEngine_GameObject = GameObject.Find( "FerriswheelEfficent:FerrisWheelTrack" ) as UnityEngine.GameObject;
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_109_UnityEngine_GameObject_previous != local_109_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_109_UnityEngine_GameObject_previous = local_109_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
      //if our game object reference was changed then we need to reset event listeners
      if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_77_UnityEngine_GameObject_previous != local_77_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_77_UnityEngine_GameObject_previous = local_77_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_94_UnityEngine_GameObject_previous != local_94_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_94_UnityEngine_GameObject_previous = local_94_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_95_UnityEngine_GameObject_previous != local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_95_UnityEngine_GameObject_previous = local_95_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_108_UnityEngine_GameObject_previous != local_108_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_108_UnityEngine_GameObject_previous = local_108_UnityEngine_GameObject;
         
         //setup new listeners
      }
      //if our game object reference was changed then we need to reset event listeners
      if ( local_109_UnityEngine_GameObject_previous != local_109_UnityEngine_GameObject || false == m_RegisteredForEvents )
      {
         //tear down old listeners
         
         local_109_UnityEngine_GameObject_previous = local_109_UnityEngine_GameObject;
         
         //setup new listeners
      }
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_0;
                  component.uScriptLateStart += Instance_uScriptLateStart_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_0;
               component.uScriptLateStart -= Instance_uScriptLateStart_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_1.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_2.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_5.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_7.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_9.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_10.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_11.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_12.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_13.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_19.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_20.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_21.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_25.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_26.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_27.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_28.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_29.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_30.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_31.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_32.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_33.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_34.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_35.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_41.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_42.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_43.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_44.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_45.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_46.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_47.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_48.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_49.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_50.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_52.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_53.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_54.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_55.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_60.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_61.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_63.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_64.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_65.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_66.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_67.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_68.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_69.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_70.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_71.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_75.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_76.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_78.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_79.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_80.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_81.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_82.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_83.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_84.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_88.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_89.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_90.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_91.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_92.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_93.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_96.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_97.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_98.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_102.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_103.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_104.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_105.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_106.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_107.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_110.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_111.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_112.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_113.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_114.SetParent(g);
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_115.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_122.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_123.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_124.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_125.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_126.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_127.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_128.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_129.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_130.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_131.SetParent(g);
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_132.SetParent(g);
      logic_uScriptAct_Delay_uScriptAct_Delay_133.SetParent(g);
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      if (true == logic_uScriptAct_Delay_DrivenDelay_9)
      {
         Relay_DrivenDelay_9();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_11)
      {
         Relay_DrivenDelay_11();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_13)
      {
         Relay_DrivenDelay_13();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_26)
      {
         Relay_DrivenDelay_26();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_28)
      {
         Relay_DrivenDelay_28();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_30)
      {
         Relay_DrivenDelay_30();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_46)
      {
         Relay_DrivenDelay_46();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_47)
      {
         Relay_DrivenDelay_47();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_48)
      {
         Relay_DrivenDelay_48();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_49)
      {
         Relay_DrivenDelay_49();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_50)
      {
         Relay_DrivenDelay_50();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_61)
      {
         Relay_DrivenDelay_61();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_64)
      {
         Relay_DrivenDelay_64();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_66)
      {
         Relay_DrivenDelay_66();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_68)
      {
         Relay_DrivenDelay_68();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_76)
      {
         Relay_DrivenDelay_76();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_79)
      {
         Relay_DrivenDelay_79();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_81)
      {
         Relay_DrivenDelay_81();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_88)
      {
         Relay_DrivenDelay_88();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_91)
      {
         Relay_DrivenDelay_91();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_92)
      {
         Relay_DrivenDelay_92();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_103)
      {
         Relay_DrivenDelay_103();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_105)
      {
         Relay_DrivenDelay_105();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_107)
      {
         Relay_DrivenDelay_107();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_127)
      {
         Relay_DrivenDelay_127();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_128)
      {
         Relay_DrivenDelay_128();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_129)
      {
         Relay_DrivenDelay_129();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_130)
      {
         Relay_DrivenDelay_130();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_131)
      {
         Relay_DrivenDelay_131();
      }
      if (true == logic_uScriptAct_Delay_DrivenDelay_133)
      {
         Relay_DrivenDelay_133();
      }
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_uScriptStart_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_0( );
   }
   
   void Instance_uScriptLateStart_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_0( );
   }
   
   void Relay_uScriptStart_0()
   {
      Relay_In_1();
      Relay_In_5();
      Relay_In_2();
      Relay_In_7();
      Relay_In_19();
      Relay_In_20();
      Relay_In_21();
      Relay_In_31();
      Relay_In_32();
      Relay_In_33();
      Relay_In_34();
      Relay_In_35();
      Relay_In_52();
      Relay_In_53();
      Relay_In_54();
      Relay_In_55();
      Relay_In_60();
      Relay_In_69();
      Relay_In_70();
      Relay_In_71();
      Relay_In_75();
      Relay_In_82();
      Relay_In_83();
      Relay_In_84();
      Relay_In_93();
      Relay_In_98();
      Relay_In_97();
      Relay_In_96();
      Relay_In_102();
      Relay_In_110();
      Relay_In_111();
      Relay_In_115();
      Relay_In_114();
      Relay_In_113();
      Relay_In_112();
   }
   
   void Relay_uScriptLateStart_0()
   {
   }
   
   void Relay_In_1()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_1.In(logic_uScriptAct_LoadMaterial_name_1, out logic_uScriptAct_LoadMaterial_materialFile_1);
      local_4_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_1;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_2()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_2.In(logic_uScriptAct_LoadMaterial_name_2, out logic_uScriptAct_LoadMaterial_materialFile_2);
      local_3_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_2;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_5()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_5.In(logic_uScriptAct_LoadMaterial_name_5, out logic_uScriptAct_LoadMaterial_materialFile_5);
      local_6_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_5;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_7()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_7 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_7 = local_4_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_7.In(logic_uScriptAct_AssignMaterial_Target_7, logic_uScriptAct_AssignMaterial_materialName_7, logic_uScriptAct_AssignMaterial_MatChannel_7);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_7.Out;
      
      if ( test_0 == true )
      {
         Relay_In_9();
      }
   }
   
   void Relay_In_9()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_9 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_9.In(logic_uScriptAct_Delay_Duration_9, logic_uScriptAct_Delay_SingleFrame_9);
      logic_uScriptAct_Delay_DrivenDelay_9 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_9.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_10();
      }
   }
   
   void Relay_Stop_9()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_9 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_9.Stop(logic_uScriptAct_Delay_Duration_9, logic_uScriptAct_Delay_SingleFrame_9);
      logic_uScriptAct_Delay_DrivenDelay_9 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_9.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_10();
      }
   }
   
   void Relay_DrivenDelay_9( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_9 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_9 = logic_uScriptAct_Delay_uScriptAct_Delay_9.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_9 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_9.AfterDelay == true )
         {
            Relay_In_10();
         }
      }
   }
   void Relay_In_10()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_10 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_10 = local_3_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_10.In(logic_uScriptAct_AssignMaterial_Target_10, logic_uScriptAct_AssignMaterial_materialName_10, logic_uScriptAct_AssignMaterial_MatChannel_10);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_10.Out;
      
      if ( test_0 == true )
      {
         Relay_In_11();
      }
   }
   
   void Relay_In_11()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_11 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_11.In(logic_uScriptAct_Delay_Duration_11, logic_uScriptAct_Delay_SingleFrame_11);
      logic_uScriptAct_Delay_DrivenDelay_11 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_11.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_Stop_11()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_11 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_11.Stop(logic_uScriptAct_Delay_Duration_11, logic_uScriptAct_Delay_SingleFrame_11);
      logic_uScriptAct_Delay_DrivenDelay_11 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_11.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_12();
      }
   }
   
   void Relay_DrivenDelay_11( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_11 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_11 = logic_uScriptAct_Delay_uScriptAct_Delay_11.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_11 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_11.AfterDelay == true )
         {
            Relay_In_12();
         }
      }
   }
   void Relay_In_12()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_12 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_12 = local_6_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_12.In(logic_uScriptAct_AssignMaterial_Target_12, logic_uScriptAct_AssignMaterial_materialName_12, logic_uScriptAct_AssignMaterial_MatChannel_12);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_12.Out;
      
      if ( test_0 == true )
      {
         Relay_In_13();
      }
   }
   
   void Relay_In_13()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_13 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_13.In(logic_uScriptAct_Delay_Duration_13, logic_uScriptAct_Delay_SingleFrame_13);
      logic_uScriptAct_Delay_DrivenDelay_13 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_13.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_Stop_13()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_13 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_13.Stop(logic_uScriptAct_Delay_Duration_13, logic_uScriptAct_Delay_SingleFrame_13);
      logic_uScriptAct_Delay_DrivenDelay_13 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_13.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_25();
      }
   }
   
   void Relay_DrivenDelay_13( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_13 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_13 = logic_uScriptAct_Delay_uScriptAct_Delay_13.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_13 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_13.AfterDelay == true )
         {
            Relay_In_25();
         }
      }
   }
   void Relay_In_19()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_19.In(logic_uScriptAct_LoadMaterial_name_19, out logic_uScriptAct_LoadMaterial_materialFile_19);
      local_22_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_19;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_20()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_20.In(logic_uScriptAct_LoadMaterial_name_20, out logic_uScriptAct_LoadMaterial_materialFile_20);
      local_23_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_20;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_21()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_21.In(logic_uScriptAct_LoadMaterial_name_21, out logic_uScriptAct_LoadMaterial_materialFile_21);
      local_24_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_21;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_25()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_25 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_25 = local_22_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_25.In(logic_uScriptAct_AssignMaterial_Target_25, logic_uScriptAct_AssignMaterial_materialName_25, logic_uScriptAct_AssignMaterial_MatChannel_25);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_25.Out;
      
      if ( test_0 == true )
      {
         Relay_In_26();
      }
   }
   
   void Relay_In_26()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_26 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_26.In(logic_uScriptAct_Delay_Duration_26, logic_uScriptAct_Delay_SingleFrame_26);
      logic_uScriptAct_Delay_DrivenDelay_26 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_26.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_27();
      }
   }
   
   void Relay_Stop_26()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_26 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_26.Stop(logic_uScriptAct_Delay_Duration_26, logic_uScriptAct_Delay_SingleFrame_26);
      logic_uScriptAct_Delay_DrivenDelay_26 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_26.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_27();
      }
   }
   
   void Relay_DrivenDelay_26( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_26 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_26 = logic_uScriptAct_Delay_uScriptAct_Delay_26.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_26 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_26.AfterDelay == true )
         {
            Relay_In_27();
         }
      }
   }
   void Relay_In_27()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_27 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_27 = local_23_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_27.In(logic_uScriptAct_AssignMaterial_Target_27, logic_uScriptAct_AssignMaterial_materialName_27, logic_uScriptAct_AssignMaterial_MatChannel_27);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_27.Out;
      
      if ( test_0 == true )
      {
         Relay_In_28();
      }
   }
   
   void Relay_In_28()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_28 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_28.In(logic_uScriptAct_Delay_Duration_28, logic_uScriptAct_Delay_SingleFrame_28);
      logic_uScriptAct_Delay_DrivenDelay_28 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_28.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_Stop_28()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_28 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_28.Stop(logic_uScriptAct_Delay_Duration_28, logic_uScriptAct_Delay_SingleFrame_28);
      logic_uScriptAct_Delay_DrivenDelay_28 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_28.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_29();
      }
   }
   
   void Relay_DrivenDelay_28( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_28 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_28 = logic_uScriptAct_Delay_uScriptAct_Delay_28.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_28 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_28.AfterDelay == true )
         {
            Relay_In_29();
         }
      }
   }
   void Relay_In_29()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_29 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_29 = local_24_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_29.In(logic_uScriptAct_AssignMaterial_Target_29, logic_uScriptAct_AssignMaterial_materialName_29, logic_uScriptAct_AssignMaterial_MatChannel_29);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_29.Out;
      
      if ( test_0 == true )
      {
         Relay_In_30();
      }
   }
   
   void Relay_In_30()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_30 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_30.In(logic_uScriptAct_Delay_Duration_30, logic_uScriptAct_Delay_SingleFrame_30);
      logic_uScriptAct_Delay_DrivenDelay_30 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_30.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_41();
      }
   }
   
   void Relay_Stop_30()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_30 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_30.Stop(logic_uScriptAct_Delay_Duration_30, logic_uScriptAct_Delay_SingleFrame_30);
      logic_uScriptAct_Delay_DrivenDelay_30 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_30.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_41();
      }
   }
   
   void Relay_DrivenDelay_30( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_30 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_30 = logic_uScriptAct_Delay_uScriptAct_Delay_30.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_30 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_30.AfterDelay == true )
         {
            Relay_In_41();
         }
      }
   }
   void Relay_In_31()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_31.In(logic_uScriptAct_LoadMaterial_name_31, out logic_uScriptAct_LoadMaterial_materialFile_31);
      local_36_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_31;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_32()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_32.In(logic_uScriptAct_LoadMaterial_name_32, out logic_uScriptAct_LoadMaterial_materialFile_32);
      local_37_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_32;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_33()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_33.In(logic_uScriptAct_LoadMaterial_name_33, out logic_uScriptAct_LoadMaterial_materialFile_33);
      local_38_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_33;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_34()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_34.In(logic_uScriptAct_LoadMaterial_name_34, out logic_uScriptAct_LoadMaterial_materialFile_34);
      local_39_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_34;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_35()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_35.In(logic_uScriptAct_LoadMaterial_name_35, out logic_uScriptAct_LoadMaterial_materialFile_35);
      local_40_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_35;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_41()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_41 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_41 = local_36_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_41.In(logic_uScriptAct_AssignMaterial_Target_41, logic_uScriptAct_AssignMaterial_materialName_41, logic_uScriptAct_AssignMaterial_MatChannel_41);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_41.Out;
      
      if ( test_0 == true )
      {
         Relay_In_46();
      }
   }
   
   void Relay_In_42()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_42 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_42 = local_37_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_42.In(logic_uScriptAct_AssignMaterial_Target_42, logic_uScriptAct_AssignMaterial_materialName_42, logic_uScriptAct_AssignMaterial_MatChannel_42);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_42.Out;
      
      if ( test_0 == true )
      {
         Relay_In_47();
      }
   }
   
   void Relay_In_43()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_43 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_43 = local_38_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_43.In(logic_uScriptAct_AssignMaterial_Target_43, logic_uScriptAct_AssignMaterial_materialName_43, logic_uScriptAct_AssignMaterial_MatChannel_43);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_43.Out;
      
      if ( test_0 == true )
      {
         Relay_In_48();
      }
   }
   
   void Relay_In_44()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_44 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_44 = local_39_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_44.In(logic_uScriptAct_AssignMaterial_Target_44, logic_uScriptAct_AssignMaterial_materialName_44, logic_uScriptAct_AssignMaterial_MatChannel_44);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_44.Out;
      
      if ( test_0 == true )
      {
         Relay_In_49();
      }
   }
   
   void Relay_In_45()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_8_UnityEngine_GameObject_previous != local_8_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_8_UnityEngine_GameObject_previous = local_8_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_8_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_18_UnityEngine_GameObject_previous != local_18_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_18_UnityEngine_GameObject_previous = local_18_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_18_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_17_UnityEngine_GameObject_previous != local_17_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_17_UnityEngine_GameObject_previous = local_17_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_17_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_16_UnityEngine_GameObject_previous != local_16_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_16_UnityEngine_GameObject_previous = local_16_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_16_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_14_UnityEngine_GameObject_previous != local_14_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_14_UnityEngine_GameObject_previous = local_14_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_14_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_15_UnityEngine_GameObject_previous != local_15_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_15_UnityEngine_GameObject_previous = local_15_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_15_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_45 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_45 = local_40_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_45.In(logic_uScriptAct_AssignMaterial_Target_45, logic_uScriptAct_AssignMaterial_materialName_45, logic_uScriptAct_AssignMaterial_MatChannel_45);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_45.Out;
      
      if ( test_0 == true )
      {
         Relay_In_50();
      }
   }
   
   void Relay_In_46()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_46 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_46.In(logic_uScriptAct_Delay_Duration_46, logic_uScriptAct_Delay_SingleFrame_46);
      logic_uScriptAct_Delay_DrivenDelay_46 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_46.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_42();
      }
   }
   
   void Relay_Stop_46()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_46 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_46.Stop(logic_uScriptAct_Delay_Duration_46, logic_uScriptAct_Delay_SingleFrame_46);
      logic_uScriptAct_Delay_DrivenDelay_46 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_46.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_42();
      }
   }
   
   void Relay_DrivenDelay_46( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_46 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_46 = logic_uScriptAct_Delay_uScriptAct_Delay_46.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_46 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_46.AfterDelay == true )
         {
            Relay_In_42();
         }
      }
   }
   void Relay_In_47()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_47.In(logic_uScriptAct_Delay_Duration_47, logic_uScriptAct_Delay_SingleFrame_47);
      logic_uScriptAct_Delay_DrivenDelay_47 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_47.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_43();
      }
   }
   
   void Relay_Stop_47()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_47.Stop(logic_uScriptAct_Delay_Duration_47, logic_uScriptAct_Delay_SingleFrame_47);
      logic_uScriptAct_Delay_DrivenDelay_47 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_47.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_43();
      }
   }
   
   void Relay_DrivenDelay_47( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_47 = logic_uScriptAct_Delay_uScriptAct_Delay_47.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_47 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_47.AfterDelay == true )
         {
            Relay_In_43();
         }
      }
   }
   void Relay_In_48()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_48 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_48.In(logic_uScriptAct_Delay_Duration_48, logic_uScriptAct_Delay_SingleFrame_48);
      logic_uScriptAct_Delay_DrivenDelay_48 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_48.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_44();
      }
   }
   
   void Relay_Stop_48()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_48 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_48.Stop(logic_uScriptAct_Delay_Duration_48, logic_uScriptAct_Delay_SingleFrame_48);
      logic_uScriptAct_Delay_DrivenDelay_48 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_48.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_44();
      }
   }
   
   void Relay_DrivenDelay_48( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_48 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_48 = logic_uScriptAct_Delay_uScriptAct_Delay_48.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_48 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_48.AfterDelay == true )
         {
            Relay_In_44();
         }
      }
   }
   void Relay_In_49()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_49 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_49.In(logic_uScriptAct_Delay_Duration_49, logic_uScriptAct_Delay_SingleFrame_49);
      logic_uScriptAct_Delay_DrivenDelay_49 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_49.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_45();
      }
   }
   
   void Relay_Stop_49()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_49 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_49.Stop(logic_uScriptAct_Delay_Duration_49, logic_uScriptAct_Delay_SingleFrame_49);
      logic_uScriptAct_Delay_DrivenDelay_49 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_49.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_45();
      }
   }
   
   void Relay_DrivenDelay_49( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_49 = SaharaTiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_49 = logic_uScriptAct_Delay_uScriptAct_Delay_49.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_49 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_49.AfterDelay == true )
         {
            Relay_In_45();
         }
      }
   }
   void Relay_In_50()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_50.In(logic_uScriptAct_Delay_Duration_50, logic_uScriptAct_Delay_SingleFrame_50);
      logic_uScriptAct_Delay_DrivenDelay_50 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_50.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_Stop_50()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_50.Stop(logic_uScriptAct_Delay_Duration_50, logic_uScriptAct_Delay_SingleFrame_50);
      logic_uScriptAct_Delay_DrivenDelay_50 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_50.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_DrivenDelay_50( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_50 = logic_uScriptAct_Delay_uScriptAct_Delay_50.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_50 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_50.AfterDelay == true )
         {
            Relay_In_7();
         }
      }
   }
   void Relay_In_52()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_52.In(logic_uScriptAct_LoadMaterial_name_52, out logic_uScriptAct_LoadMaterial_materialFile_52);
      local_56_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_52;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_53()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_53.In(logic_uScriptAct_LoadMaterial_name_53, out logic_uScriptAct_LoadMaterial_materialFile_53);
      local_57_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_53;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_54()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_54.In(logic_uScriptAct_LoadMaterial_name_54, out logic_uScriptAct_LoadMaterial_materialFile_54);
      local_58_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_54;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_55()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_55.In(logic_uScriptAct_LoadMaterial_name_55, out logic_uScriptAct_LoadMaterial_materialFile_55);
      local_59_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_55;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_60()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_60 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_60 = local_56_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_60.In(logic_uScriptAct_AssignMaterial_Target_60, logic_uScriptAct_AssignMaterial_materialName_60, logic_uScriptAct_AssignMaterial_MatChannel_60);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_60.Out;
      
      if ( test_0 == true )
      {
         Relay_In_61();
      }
   }
   
   void Relay_In_61()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_61 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_61.In(logic_uScriptAct_Delay_Duration_61, logic_uScriptAct_Delay_SingleFrame_61);
      logic_uScriptAct_Delay_DrivenDelay_61 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_61.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_63();
      }
   }
   
   void Relay_Stop_61()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_61 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_61.Stop(logic_uScriptAct_Delay_Duration_61, logic_uScriptAct_Delay_SingleFrame_61);
      logic_uScriptAct_Delay_DrivenDelay_61 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_61.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_63();
      }
   }
   
   void Relay_DrivenDelay_61( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_61 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_61 = logic_uScriptAct_Delay_uScriptAct_Delay_61.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_61 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_61.AfterDelay == true )
         {
            Relay_In_63();
         }
      }
   }
   void Relay_In_63()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_63 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_63 = local_57_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_63.In(logic_uScriptAct_AssignMaterial_Target_63, logic_uScriptAct_AssignMaterial_materialName_63, logic_uScriptAct_AssignMaterial_MatChannel_63);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_63.Out;
      
      if ( test_0 == true )
      {
         Relay_In_64();
      }
   }
   
   void Relay_In_64()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_64 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_64.In(logic_uScriptAct_Delay_Duration_64, logic_uScriptAct_Delay_SingleFrame_64);
      logic_uScriptAct_Delay_DrivenDelay_64 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_64.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_65();
      }
   }
   
   void Relay_Stop_64()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_64 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_64.Stop(logic_uScriptAct_Delay_Duration_64, logic_uScriptAct_Delay_SingleFrame_64);
      logic_uScriptAct_Delay_DrivenDelay_64 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_64.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_65();
      }
   }
   
   void Relay_DrivenDelay_64( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_64 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_64 = logic_uScriptAct_Delay_uScriptAct_Delay_64.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_64 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_64.AfterDelay == true )
         {
            Relay_In_65();
         }
      }
   }
   void Relay_In_65()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_65 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_65 = local_58_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_65.In(logic_uScriptAct_AssignMaterial_Target_65, logic_uScriptAct_AssignMaterial_materialName_65, logic_uScriptAct_AssignMaterial_MatChannel_65);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_65.Out;
      
      if ( test_0 == true )
      {
         Relay_In_66();
      }
   }
   
   void Relay_In_66()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_66 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_66.In(logic_uScriptAct_Delay_Duration_66, logic_uScriptAct_Delay_SingleFrame_66);
      logic_uScriptAct_Delay_DrivenDelay_66 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_66.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_67();
      }
   }
   
   void Relay_Stop_66()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_66 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_66.Stop(logic_uScriptAct_Delay_Duration_66, logic_uScriptAct_Delay_SingleFrame_66);
      logic_uScriptAct_Delay_DrivenDelay_66 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_66.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_67();
      }
   }
   
   void Relay_DrivenDelay_66( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_66 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_66 = logic_uScriptAct_Delay_uScriptAct_Delay_66.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_66 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_66.AfterDelay == true )
         {
            Relay_In_67();
         }
      }
   }
   void Relay_In_67()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_67 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_67 = local_59_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_67.In(logic_uScriptAct_AssignMaterial_Target_67, logic_uScriptAct_AssignMaterial_materialName_67, logic_uScriptAct_AssignMaterial_MatChannel_67);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_67.Out;
      
      if ( test_0 == true )
      {
         Relay_In_68();
      }
   }
   
   void Relay_In_68()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_68 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_68.In(logic_uScriptAct_Delay_Duration_68, logic_uScriptAct_Delay_SingleFrame_68);
      logic_uScriptAct_Delay_DrivenDelay_68 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_68.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_122();
      }
   }
   
   void Relay_Stop_68()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_68 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_68.Stop(logic_uScriptAct_Delay_Duration_68, logic_uScriptAct_Delay_SingleFrame_68);
      logic_uScriptAct_Delay_DrivenDelay_68 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_68.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_122();
      }
   }
   
   void Relay_DrivenDelay_68( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_68 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_68 = logic_uScriptAct_Delay_uScriptAct_Delay_68.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_68 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_68.AfterDelay == true )
         {
            Relay_In_122();
         }
      }
   }
   void Relay_In_69()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_69.In(logic_uScriptAct_LoadMaterial_name_69, out logic_uScriptAct_LoadMaterial_materialFile_69);
      local_72_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_69;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_70()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_70.In(logic_uScriptAct_LoadMaterial_name_70, out logic_uScriptAct_LoadMaterial_materialFile_70);
      local_73_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_70;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_71()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_71.In(logic_uScriptAct_LoadMaterial_name_71, out logic_uScriptAct_LoadMaterial_materialFile_71);
      local_74_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_71;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_75()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_77_UnityEngine_GameObject_previous != local_77_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_77_UnityEngine_GameObject_previous = local_77_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_77_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_75 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_75 = local_72_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_75.In(logic_uScriptAct_AssignMaterial_Target_75, logic_uScriptAct_AssignMaterial_materialName_75, logic_uScriptAct_AssignMaterial_MatChannel_75);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_75.Out;
      
      if ( test_0 == true )
      {
         Relay_In_76();
      }
   }
   
   void Relay_In_76()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_76.In(logic_uScriptAct_Delay_Duration_76, logic_uScriptAct_Delay_SingleFrame_76);
      logic_uScriptAct_Delay_DrivenDelay_76 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_76.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_78();
      }
   }
   
   void Relay_Stop_76()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_76.Stop(logic_uScriptAct_Delay_Duration_76, logic_uScriptAct_Delay_SingleFrame_76);
      logic_uScriptAct_Delay_DrivenDelay_76 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_76.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_78();
      }
   }
   
   void Relay_DrivenDelay_76( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_76 = logic_uScriptAct_Delay_uScriptAct_Delay_76.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_76 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_76.AfterDelay == true )
         {
            Relay_In_78();
         }
      }
   }
   void Relay_In_78()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_77_UnityEngine_GameObject_previous != local_77_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_77_UnityEngine_GameObject_previous = local_77_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_77_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_78 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_78 = local_73_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_78.In(logic_uScriptAct_AssignMaterial_Target_78, logic_uScriptAct_AssignMaterial_materialName_78, logic_uScriptAct_AssignMaterial_MatChannel_78);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_78.Out;
      
      if ( test_0 == true )
      {
         Relay_In_79();
      }
   }
   
   void Relay_In_79()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_79.In(logic_uScriptAct_Delay_Duration_79, logic_uScriptAct_Delay_SingleFrame_79);
      logic_uScriptAct_Delay_DrivenDelay_79 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_79.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_80();
      }
   }
   
   void Relay_Stop_79()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_79.Stop(logic_uScriptAct_Delay_Duration_79, logic_uScriptAct_Delay_SingleFrame_79);
      logic_uScriptAct_Delay_DrivenDelay_79 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_79.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_80();
      }
   }
   
   void Relay_DrivenDelay_79( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_79 = logic_uScriptAct_Delay_uScriptAct_Delay_79.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_79 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_79.AfterDelay == true )
         {
            Relay_In_80();
         }
      }
   }
   void Relay_In_80()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_77_UnityEngine_GameObject_previous != local_77_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_77_UnityEngine_GameObject_previous = local_77_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_77_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_80 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_80 = local_74_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_80.In(logic_uScriptAct_AssignMaterial_Target_80, logic_uScriptAct_AssignMaterial_materialName_80, logic_uScriptAct_AssignMaterial_MatChannel_80);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_80.Out;
      
      if ( test_0 == true )
      {
         Relay_In_81();
      }
   }
   
   void Relay_In_81()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_81.In(logic_uScriptAct_Delay_Duration_81, logic_uScriptAct_Delay_SingleFrame_81);
      logic_uScriptAct_Delay_DrivenDelay_81 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_81.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_75();
      }
   }
   
   void Relay_Stop_81()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_81.Stop(logic_uScriptAct_Delay_Duration_81, logic_uScriptAct_Delay_SingleFrame_81);
      logic_uScriptAct_Delay_DrivenDelay_81 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_81.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_75();
      }
   }
   
   void Relay_DrivenDelay_81( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_81 = logic_uScriptAct_Delay_uScriptAct_Delay_81.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_81 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_81.AfterDelay == true )
         {
            Relay_In_75();
         }
      }
   }
   void Relay_In_82()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_82.In(logic_uScriptAct_LoadMaterial_name_82, out logic_uScriptAct_LoadMaterial_materialFile_82);
      local_85_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_82;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_83()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_83.In(logic_uScriptAct_LoadMaterial_name_83, out logic_uScriptAct_LoadMaterial_materialFile_83);
      local_86_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_83;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_84()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_84.In(logic_uScriptAct_LoadMaterial_name_84, out logic_uScriptAct_LoadMaterial_materialFile_84);
      local_87_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_84;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_88()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_88.In(logic_uScriptAct_Delay_Duration_88, logic_uScriptAct_Delay_SingleFrame_88);
      logic_uScriptAct_Delay_DrivenDelay_88 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_88.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_89();
      }
   }
   
   void Relay_Stop_88()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_88.Stop(logic_uScriptAct_Delay_Duration_88, logic_uScriptAct_Delay_SingleFrame_88);
      logic_uScriptAct_Delay_DrivenDelay_88 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_88.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_89();
      }
   }
   
   void Relay_DrivenDelay_88( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_88 = logic_uScriptAct_Delay_uScriptAct_Delay_88.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_88 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_88.AfterDelay == true )
         {
            Relay_In_89();
         }
      }
   }
   void Relay_In_89()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_95_UnityEngine_GameObject_previous != local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_95_UnityEngine_GameObject_previous = local_95_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_95_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_94_UnityEngine_GameObject_previous != local_94_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_94_UnityEngine_GameObject_previous = local_94_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_94_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_89 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_89 = local_87_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_89.In(logic_uScriptAct_AssignMaterial_Target_89, logic_uScriptAct_AssignMaterial_materialName_89, logic_uScriptAct_AssignMaterial_MatChannel_89);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_89.Out;
      
      if ( test_0 == true )
      {
         Relay_In_92();
      }
   }
   
   void Relay_In_90()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_94_UnityEngine_GameObject_previous != local_94_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_94_UnityEngine_GameObject_previous = local_94_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_94_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_95_UnityEngine_GameObject_previous != local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_95_UnityEngine_GameObject_previous = local_95_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_95_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_90 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_90 = local_86_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_90.In(logic_uScriptAct_AssignMaterial_Target_90, logic_uScriptAct_AssignMaterial_materialName_90, logic_uScriptAct_AssignMaterial_MatChannel_90);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_90.Out;
      
      if ( test_0 == true )
      {
         Relay_In_88();
      }
   }
   
   void Relay_In_91()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_91.In(logic_uScriptAct_Delay_Duration_91, logic_uScriptAct_Delay_SingleFrame_91);
      logic_uScriptAct_Delay_DrivenDelay_91 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_91.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_90();
      }
   }
   
   void Relay_Stop_91()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_91.Stop(logic_uScriptAct_Delay_Duration_91, logic_uScriptAct_Delay_SingleFrame_91);
      logic_uScriptAct_Delay_DrivenDelay_91 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_91.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_90();
      }
   }
   
   void Relay_DrivenDelay_91( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_91 = logic_uScriptAct_Delay_uScriptAct_Delay_91.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_91 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_91.AfterDelay == true )
         {
            Relay_In_90();
         }
      }
   }
   void Relay_In_92()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_92.In(logic_uScriptAct_Delay_Duration_92, logic_uScriptAct_Delay_SingleFrame_92);
      logic_uScriptAct_Delay_DrivenDelay_92 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_92.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_93();
      }
   }
   
   void Relay_Stop_92()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_92.Stop(logic_uScriptAct_Delay_Duration_92, logic_uScriptAct_Delay_SingleFrame_92);
      logic_uScriptAct_Delay_DrivenDelay_92 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_92.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_93();
      }
   }
   
   void Relay_DrivenDelay_92( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_92 = logic_uScriptAct_Delay_uScriptAct_Delay_92.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_92 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_92.AfterDelay == true )
         {
            Relay_In_93();
         }
      }
   }
   void Relay_In_93()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_94_UnityEngine_GameObject_previous != local_94_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_94_UnityEngine_GameObject_previous = local_94_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_94_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_95_UnityEngine_GameObject_previous != local_95_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_95_UnityEngine_GameObject_previous = local_95_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_95_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_93 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_93 = local_85_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_93.In(logic_uScriptAct_AssignMaterial_Target_93, logic_uScriptAct_AssignMaterial_materialName_93, logic_uScriptAct_AssignMaterial_MatChannel_93);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_93.Out;
      
      if ( test_0 == true )
      {
         Relay_In_91();
      }
   }
   
   void Relay_In_96()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_96.In(logic_uScriptAct_LoadMaterial_name_96, out logic_uScriptAct_LoadMaterial_materialFile_96);
      local_99_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_96;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_97()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_97.In(logic_uScriptAct_LoadMaterial_name_97, out logic_uScriptAct_LoadMaterial_materialFile_97);
      local_100_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_97;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_98()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_98.In(logic_uScriptAct_LoadMaterial_name_98, out logic_uScriptAct_LoadMaterial_materialFile_98);
      local_101_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_98;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_102()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_108_UnityEngine_GameObject_previous != local_108_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_108_UnityEngine_GameObject_previous = local_108_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_108_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_109_UnityEngine_GameObject_previous != local_109_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_109_UnityEngine_GameObject_previous = local_109_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_109_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_102 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_102 = local_99_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_102.In(logic_uScriptAct_AssignMaterial_Target_102, logic_uScriptAct_AssignMaterial_materialName_102, logic_uScriptAct_AssignMaterial_MatChannel_102);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_102.Out;
      
      if ( test_0 == true )
      {
         Relay_In_103();
      }
   }
   
   void Relay_In_103()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_103.In(logic_uScriptAct_Delay_Duration_103, logic_uScriptAct_Delay_SingleFrame_103);
      logic_uScriptAct_Delay_DrivenDelay_103 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_103.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_104();
      }
   }
   
   void Relay_Stop_103()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_103.Stop(logic_uScriptAct_Delay_Duration_103, logic_uScriptAct_Delay_SingleFrame_103);
      logic_uScriptAct_Delay_DrivenDelay_103 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_103.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_104();
      }
   }
   
   void Relay_DrivenDelay_103( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_103 = logic_uScriptAct_Delay_uScriptAct_Delay_103.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_103 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_103.AfterDelay == true )
         {
            Relay_In_104();
         }
      }
   }
   void Relay_In_104()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_108_UnityEngine_GameObject_previous != local_108_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_108_UnityEngine_GameObject_previous = local_108_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_108_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_109_UnityEngine_GameObject_previous != local_109_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_109_UnityEngine_GameObject_previous = local_109_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_109_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_104 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_104 = local_100_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_104.In(logic_uScriptAct_AssignMaterial_Target_104, logic_uScriptAct_AssignMaterial_materialName_104, logic_uScriptAct_AssignMaterial_MatChannel_104);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_104.Out;
      
      if ( test_0 == true )
      {
         Relay_In_107();
      }
   }
   
   void Relay_In_105()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_105.In(logic_uScriptAct_Delay_Duration_105, logic_uScriptAct_Delay_SingleFrame_105);
      logic_uScriptAct_Delay_DrivenDelay_105 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_105.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_102();
      }
   }
   
   void Relay_Stop_105()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_105.Stop(logic_uScriptAct_Delay_Duration_105, logic_uScriptAct_Delay_SingleFrame_105);
      logic_uScriptAct_Delay_DrivenDelay_105 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_105.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_102();
      }
   }
   
   void Relay_DrivenDelay_105( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_105 = logic_uScriptAct_Delay_uScriptAct_Delay_105.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_105 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_105.AfterDelay == true )
         {
            Relay_In_102();
         }
      }
   }
   void Relay_In_106()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_108_UnityEngine_GameObject_previous != local_108_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_108_UnityEngine_GameObject_previous = local_108_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_108_UnityEngine_GameObject);
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_109_UnityEngine_GameObject_previous != local_109_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_109_UnityEngine_GameObject_previous = local_109_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_109_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_106 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_106 = local_101_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_106.In(logic_uScriptAct_AssignMaterial_Target_106, logic_uScriptAct_AssignMaterial_materialName_106, logic_uScriptAct_AssignMaterial_MatChannel_106);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_106.Out;
      
      if ( test_0 == true )
      {
         Relay_In_105();
      }
   }
   
   void Relay_In_107()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_107.In(logic_uScriptAct_Delay_Duration_107, logic_uScriptAct_Delay_SingleFrame_107);
      logic_uScriptAct_Delay_DrivenDelay_107 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_107.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_106();
      }
   }
   
   void Relay_Stop_107()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_107.Stop(logic_uScriptAct_Delay_Duration_107, logic_uScriptAct_Delay_SingleFrame_107);
      logic_uScriptAct_Delay_DrivenDelay_107 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_107.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_106();
      }
   }
   
   void Relay_DrivenDelay_107( )
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_107 = logic_uScriptAct_Delay_uScriptAct_Delay_107.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_107 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_107.AfterDelay == true )
         {
            Relay_In_106();
         }
      }
   }
   void Relay_In_110()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_110.In(logic_uScriptAct_LoadMaterial_name_110, out logic_uScriptAct_LoadMaterial_materialFile_110);
      local_116_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_110;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_111()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_111.In(logic_uScriptAct_LoadMaterial_name_111, out logic_uScriptAct_LoadMaterial_materialFile_111);
      local_117_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_111;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_112()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_112.In(logic_uScriptAct_LoadMaterial_name_112, out logic_uScriptAct_LoadMaterial_materialFile_112);
      local_118_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_112;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_113()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_113.In(logic_uScriptAct_LoadMaterial_name_113, out logic_uScriptAct_LoadMaterial_materialFile_113);
      local_119_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_113;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_114()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_114.In(logic_uScriptAct_LoadMaterial_name_114, out logic_uScriptAct_LoadMaterial_materialFile_114);
      local_120_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_114;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_115()
   {
      {
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadMaterial_uScriptAct_LoadMaterial_115.In(logic_uScriptAct_LoadMaterial_name_115, out logic_uScriptAct_LoadMaterial_materialFile_115);
      local_121_UnityEngine_Material = logic_uScriptAct_LoadMaterial_materialFile_115;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_In_122()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_122 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_122 = local_116_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_122.In(logic_uScriptAct_AssignMaterial_Target_122, logic_uScriptAct_AssignMaterial_materialName_122, logic_uScriptAct_AssignMaterial_MatChannel_122);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_122.Out;
      
      if ( test_0 == true )
      {
         Relay_In_127();
      }
   }
   
   void Relay_In_123()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_123 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_123 = local_117_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_123.In(logic_uScriptAct_AssignMaterial_Target_123, logic_uScriptAct_AssignMaterial_materialName_123, logic_uScriptAct_AssignMaterial_MatChannel_123);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_123.Out;
      
      if ( test_0 == true )
      {
         Relay_In_128();
      }
   }
   
   void Relay_In_124()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_124 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_124 = local_118_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_124.In(logic_uScriptAct_AssignMaterial_Target_124, logic_uScriptAct_AssignMaterial_materialName_124, logic_uScriptAct_AssignMaterial_MatChannel_124);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_124.Out;
      
      if ( test_0 == true )
      {
         Relay_In_129();
      }
   }
   
   void Relay_In_125()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_125 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_125 = local_119_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_125.In(logic_uScriptAct_AssignMaterial_Target_125, logic_uScriptAct_AssignMaterial_materialName_125, logic_uScriptAct_AssignMaterial_MatChannel_125);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_125.Out;
      
      if ( test_0 == true )
      {
         Relay_In_130();
      }
   }
   
   void Relay_In_126()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_126 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_126 = local_120_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_126.In(logic_uScriptAct_AssignMaterial_Target_126, logic_uScriptAct_AssignMaterial_materialName_126, logic_uScriptAct_AssignMaterial_MatChannel_126);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_126.Out;
      
      if ( test_0 == true )
      {
         Relay_In_131();
      }
   }
   
   void Relay_In_127()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_127 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_127.In(logic_uScriptAct_Delay_Duration_127, logic_uScriptAct_Delay_SingleFrame_127);
      logic_uScriptAct_Delay_DrivenDelay_127 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_127.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_123();
      }
   }
   
   void Relay_Stop_127()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_127 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_127.Stop(logic_uScriptAct_Delay_Duration_127, logic_uScriptAct_Delay_SingleFrame_127);
      logic_uScriptAct_Delay_DrivenDelay_127 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_127.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_123();
      }
   }
   
   void Relay_DrivenDelay_127( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_127 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_127 = logic_uScriptAct_Delay_uScriptAct_Delay_127.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_127 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_127.AfterDelay == true )
         {
            Relay_In_123();
         }
      }
   }
   void Relay_In_128()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_128 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_128.In(logic_uScriptAct_Delay_Duration_128, logic_uScriptAct_Delay_SingleFrame_128);
      logic_uScriptAct_Delay_DrivenDelay_128 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_128.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_124();
      }
   }
   
   void Relay_Stop_128()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_128 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_128.Stop(logic_uScriptAct_Delay_Duration_128, logic_uScriptAct_Delay_SingleFrame_128);
      logic_uScriptAct_Delay_DrivenDelay_128 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_128.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_124();
      }
   }
   
   void Relay_DrivenDelay_128( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_128 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_128 = logic_uScriptAct_Delay_uScriptAct_Delay_128.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_128 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_128.AfterDelay == true )
         {
            Relay_In_124();
         }
      }
   }
   void Relay_In_129()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_129 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_129.In(logic_uScriptAct_Delay_Duration_129, logic_uScriptAct_Delay_SingleFrame_129);
      logic_uScriptAct_Delay_DrivenDelay_129 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_129.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_125();
      }
   }
   
   void Relay_Stop_129()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_129 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_129.Stop(logic_uScriptAct_Delay_Duration_129, logic_uScriptAct_Delay_SingleFrame_129);
      logic_uScriptAct_Delay_DrivenDelay_129 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_129.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_125();
      }
   }
   
   void Relay_DrivenDelay_129( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_129 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_129 = logic_uScriptAct_Delay_uScriptAct_Delay_129.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_129 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_129.AfterDelay == true )
         {
            Relay_In_125();
         }
      }
   }
   void Relay_In_130()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_130 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_130.In(logic_uScriptAct_Delay_Duration_130, logic_uScriptAct_Delay_SingleFrame_130);
      logic_uScriptAct_Delay_DrivenDelay_130 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_130.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_126();
      }
   }
   
   void Relay_Stop_130()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_130 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_130.Stop(logic_uScriptAct_Delay_Duration_130, logic_uScriptAct_Delay_SingleFrame_130);
      logic_uScriptAct_Delay_DrivenDelay_130 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_130.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_126();
      }
   }
   
   void Relay_DrivenDelay_130( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_130 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_130 = logic_uScriptAct_Delay_uScriptAct_Delay_130.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_130 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_130.AfterDelay == true )
         {
            Relay_In_126();
         }
      }
   }
   void Relay_In_131()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_131 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_131.In(logic_uScriptAct_Delay_Duration_131, logic_uScriptAct_Delay_SingleFrame_131);
      logic_uScriptAct_Delay_DrivenDelay_131 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_131.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_132();
      }
   }
   
   void Relay_Stop_131()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_131 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_131.Stop(logic_uScriptAct_Delay_Duration_131, logic_uScriptAct_Delay_SingleFrame_131);
      logic_uScriptAct_Delay_DrivenDelay_131 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_131.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_132();
      }
   }
   
   void Relay_DrivenDelay_131( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_131 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_131 = logic_uScriptAct_Delay_uScriptAct_Delay_131.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_131 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_131.AfterDelay == true )
         {
            Relay_In_132();
         }
      }
   }
   void Relay_In_132()
   {
      {
         {
            List<UnityEngine.GameObject> properties = new List<UnityEngine.GameObject>();
            {
               //if our game object reference was changed then we need to reset event listeners
               if ( local_62_UnityEngine_GameObject_previous != local_62_UnityEngine_GameObject || false == m_RegisteredForEvents )
               {
                  //tear down old listeners
                  
                  local_62_UnityEngine_GameObject_previous = local_62_UnityEngine_GameObject;
                  
                  //setup new listeners
               }
            }
            properties.Add((UnityEngine.GameObject)local_62_UnityEngine_GameObject);
            logic_uScriptAct_AssignMaterial_Target_132 = properties.ToArray();
         }
         {
            logic_uScriptAct_AssignMaterial_materialName_132 = local_121_UnityEngine_Material;
            
         }
         {
         }
      }
      logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_132.In(logic_uScriptAct_AssignMaterial_Target_132, logic_uScriptAct_AssignMaterial_materialName_132, logic_uScriptAct_AssignMaterial_MatChannel_132);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_AssignMaterial_uScriptAct_AssignMaterial_132.Out;
      
      if ( test_0 == true )
      {
         Relay_In_133();
      }
   }
   
   void Relay_In_133()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_133 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_133.In(logic_uScriptAct_Delay_Duration_133, logic_uScriptAct_Delay_SingleFrame_133);
      logic_uScriptAct_Delay_DrivenDelay_133 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_133.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_60();
      }
   }
   
   void Relay_Stop_133()
   {
      {
         {
            logic_uScriptAct_Delay_Duration_133 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_uScriptAct_Delay_133.Stop(logic_uScriptAct_Delay_Duration_133, logic_uScriptAct_Delay_SingleFrame_133);
      logic_uScriptAct_Delay_DrivenDelay_133 = true;
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_Delay_uScriptAct_Delay_133.AfterDelay;
      
      if ( test_0 == true )
      {
         Relay_In_60();
      }
   }
   
   void Relay_DrivenDelay_133( )
   {
      {
         {
            logic_uScriptAct_Delay_Duration_133 = Mainstagetiming;
            
         }
         {
         }
      }
      logic_uScriptAct_Delay_DrivenDelay_133 = logic_uScriptAct_Delay_uScriptAct_Delay_133.DrivenDelay();
      if ( true == logic_uScriptAct_Delay_DrivenDelay_133 )
      {
         if ( logic_uScriptAct_Delay_uScriptAct_Delay_133.AfterDelay == true )
         {
            Relay_In_60();
         }
      }
   }
}
