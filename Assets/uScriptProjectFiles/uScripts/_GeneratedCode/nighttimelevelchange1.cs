//uScript Generated Code - Build 1.0.2522
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class nighttimelevelchange1 : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_0 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_0 = UnityEngine.KeyCode.JoystickButton0;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_0 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_0 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_0 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_2 = UnityEngine.KeyCode.JoystickButton1;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_2 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_2 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_2 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_3 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_3 = "Noontime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_3 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_3 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_3 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_4 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_4 = "Sunsettime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_4 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_4 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_4 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_1 = null;
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_1 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_1 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_1 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_1.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_1.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_1;
                  component.uScriptLateStart += Instance_uScriptLateStart_1;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_1 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_1.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_1;
               component.uScriptLateStart -= Instance_uScriptLateStart_1;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_0.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_3.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_4.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_3.Loaded += uScriptAct_LoadLevel_Loaded_3;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_4.Loaded += uScriptAct_LoadLevel_Loaded_4;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_3.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_4.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_3.Loaded -= uScriptAct_LoadLevel_Loaded_3;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_4.Loaded -= uScriptAct_LoadLevel_Loaded_4;
   }
   
   void Instance_uScriptStart_1(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_1( );
   }
   
   void Instance_uScriptLateStart_1(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_1( );
   }
   
   void uScriptAct_LoadLevel_Loaded_3(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_3( );
   }
   
   void uScriptAct_LoadLevel_Loaded_4(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_4( );
   }
   
   void Relay_In_0()
   {
      {
         {
         }
      }
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_0.In(logic_uScriptAct_OnInputEventFilter_KeyCode_0);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_0.KeyUp;
      
      if ( test_0 == true )
      {
         Relay_In_3();
      }
   }
   
   void Relay_uScriptStart_1()
   {
      Relay_In_0();
      Relay_In_2();
   }
   
   void Relay_uScriptLateStart_1()
   {
   }
   
   void Relay_In_2()
   {
      {
         {
         }
      }
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2.In(logic_uScriptAct_OnInputEventFilter_KeyCode_2);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2.KeyUp;
      
      if ( test_0 == true )
      {
         Relay_In_4();
      }
   }
   
   void Relay_Loaded_3()
   {
   }
   
   void Relay_In_3()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_3.In(logic_uScriptAct_LoadLevel_name_3, logic_uScriptAct_LoadLevel_destroyOtherObjects_3, logic_uScriptAct_LoadLevel_blockUntilLoaded_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Loaded_4()
   {
   }
   
   void Relay_In_4()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_4.In(logic_uScriptAct_LoadLevel_name_4, logic_uScriptAct_LoadLevel_destroyOtherObjects_4, logic_uScriptAct_LoadLevel_blockUntilLoaded_4);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
