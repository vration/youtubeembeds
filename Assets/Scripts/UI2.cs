﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UI2 : MonoBehaviour 
{
	public static UI2 Instance = null;
	
	private void Awake()
	{
		if( Instance == null )
		{
			Instance = this;
		}
		
		if( UI.Instance != null )
		{
			UI.Instance = null;
		}
		
		for( int i = 0; i < _Screens.Length; ++i )
		{
			_Screens[i].gameObject.SetActive( _Screens[i] == _CurrentWebTexture );
		}
		
		UWKWebView.DisableInput();
		Invoke ( "SelectFirstScreen", 1 );
	}
	
	private void SelectFirstScreen()
	{
		//click on new WebTexture
		//UWKWebView.DisableInput();
		UWKWebView.EnableInput();
		_CurrentWebTexture.ClickWebTexture();
		UWKWebView.DisableInput();
	}
			
	public void LoadNighttimeScene()
	{	
		Application.LoadLevel( "Nighttime" );
	}
	
	//called by UI buttons
	public void SelectScreen( TriggerWebTextureClick inWebTextureClick )
	{
		CancelInvoke ( "SelectFirstScreen" );
		
		if( _CurrentWebTexture != inWebTextureClick )
		{
			//click off playing WebTexure
			if( _CurrentWebTexture != null )
			{
				UWKWebView.EnableInput();
				_CurrentWebTexture.ClickWebTexture();
				UWKWebView.DisableInput();
				
				_CurrentWebTexture.gameObject.SetActive( false );
			}
			
			//click on new WebTexture
			_CurrentWebTexture = inWebTextureClick;
			_CurrentWebTexture.gameObject.SetActive( true );
			
			_CurrentWebTexture.DelayedClickWebTexture();
		}
	}
	
	//called by UWKWebView mouse up
	/*public void RegisterWebTextureClick( TriggerWebTextureClick inWebTextureClick )
	{
		CancelInvoke ( "SelectFirstScreen" );
		
		if( _CurrentWebTexture != inWebTextureClick )
		{
			//click off playing WebTexure
			if( _CurrentWebTexture != null )
			{
				_CurrentWebTexture.ClickWebTexture();
			}
			
			//click on new WebTexture
			_CurrentWebTexture = inWebTextureClick;
		}
		else
		{
			_CurrentWebTexture = null;//if already playing, deselect it
		}
	}*/
	
	[Header("Set This To First Screen")]
	[SerializeField] private TriggerWebTextureClick		_CurrentWebTexture = null;
	[SerializeField] TriggerWebTextureClick[]			_Screens = new TriggerWebTextureClick[0];
}
