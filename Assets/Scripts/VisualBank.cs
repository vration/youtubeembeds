﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class AnimClipArray
{
	public Animation			AnimationComponent = null;
	public AnimationClip[] 		AnimClips;
}

public class VisualBank : Bank 
{
	[HideInInspector] public string 				QueuedClip = null;
	[HideInInspector] public string 				PlayingClip = null;
	[HideInInspector] public bool					FlagToStopClip;

	public void SetQueuedClip( int inClipIndex )
	{
		//Debug.Log ( "SetQueuedClip()  to : " + _AnimClips[ inClipIndex ] );
		QueuedClip = _AnimClips[ inClipIndex ];

		_QueuedSecondaryAnimClips.Clear();

		//queue secondary animations
		for( int i = 0; i < _SecondaryAnimations.Count; ++i )
		{
			AnimClipArray animClipArray = _SecondaryAnimations[ i ];

			if( animClipArray.AnimationComponent != null )
			{
				if( animClipArray.AnimClips.Length > 0 )
				{
					_QueuedSecondaryAnimClips.Add( animClipArray.AnimClips[ inClipIndex ] );
				}
				else
				{
					_QueuedSecondaryAnimClips.Add( null );
				}
			}
		}
	}
	
	public void QueueClipToStop()
	{
		//Debug.Log ( "QueueClipToStop()");
		FlagToStopClip = true;
	}
	
	public void PlayQueuedClip()
	{
		//Debug.Log ( "PlayQueuedClip() ()   QueuedClip : " + QueuedClip );
		PlayingClip = QueuedClip;
		QueuedClip = string.Empty;

		_SpotlightController.PlayAnim( PlayingClip );

		//queue secondary animations
		for( int i = 0; i < _SecondaryAnimations.Count; ++i )
		{
			AnimClipArray animClipArray = _SecondaryAnimations[ i ];
			
			if( animClipArray.AnimationComponent != null )
			{
				AnimationClip clip = _QueuedSecondaryAnimClips[ i ];
				animClipArray.AnimationComponent.Stop();

				if( clip != null )
				{
					animClipArray.AnimationComponent.clip = clip;
					animClipArray.AnimationComponent.Play();
				}
			}
		}
	}
	
	/*
	public void PlayClip( int inClipIndex ) 
	{
		_PlayingClip = _AudioClips[ inClipIndex ];

		audio.clip = _PlayingClip;
		audio.loop = true;
	}*/
	
	public void StopClip()
	{
		//Debug.Log( "StopClip()  " + PlayingClip );
		FlagToStopClip = false;
		_SpotlightController.StopAnim();

		PlayingClip = string.Empty;
		QueuedClip = string.Empty;

		//stop secondary animations
		for( int i = 0; i < _SecondaryAnimations.Count; ++i )
		{
			AnimClipArray animClipArray = _SecondaryAnimations[ i ];
			
			if( animClipArray.AnimationComponent != null )
			{
				animClipArray.AnimationComponent.Stop();
				animClipArray.AnimationComponent.clip = null;

				MaterialChanger mc = animClipArray.AnimationComponent.GetComponent<MaterialChanger>();

				if( mc != null )
				{
					mc.ClearMaterial();
				}
			}
		}

		_QueuedSecondaryAnimClips.Clear();
	}

	/*
	public float GetClipLength( int inClipIndex )
	{
		return _VisualClips[ inClipIndex ].Duration;
	}*/
	
	public float GetCurrentClipLength()
	{
		return _SpotlightController.GetCurrentAnimationLength();
	}

	private List<AnimationClip>							_QueuedSecondaryAnimClips = new List<AnimationClip>();

	[SerializeField] private SpotlightController		_SpotlightController = null;
	[SerializeField] private List<string>				_AnimClips = new List<string>();
	[SerializeField] private List<AnimClipArray>		_SecondaryAnimations = new List<AnimClipArray>();
}