﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class FerrisWheelEnterZone : MonoBehaviour 
{
	private void Start()
	{
		_EnterExitButton.SetActive( false );
		_FirstPersonController = GameObject.FindGameObjectWithTag( "FPC" );
	}

	private void OnTriggerEnter( Collider inCollider ) 
	{
		if( ! _IsTriggered )
		{
			_IsTriggered = true;

			_SequencerButton.SetActive( false );

			//show button
			_EnterExitButton.SetActive( true );
			_ButtonLabel.text = "Ride";
		}
	}

	private void OnTriggerExit( Collider inCollider )
	{
		if( _IsTriggered && ! _UserIsRidingFerrisWheel )
		{
			_IsTriggered = false;

			//only in night scene
			if( Application.loadedLevelName == "Nighttime" )
			{
				_SequencerButton.gameObject.SetActive( true );
			}

			//hide button
			_EnterExitButton.SetActive( false );
		}
	}

	public void HandleButtonPress()
	{
		if( ! _UserIsRidingFerrisWheel )
		{
			_UserIsRidingFerrisWheel = true;
			PutPlayerOnFerrisWheel();
		}
		else
		{
			_UserIsRidingFerrisWheel = false;
			ReturnPlayerToDeparturePoint();
		}
	}

	private void PutPlayerOnFerrisWheel()
	{
		_ButtonLabel.text = "Exit";

		_SequencerButton.gameObject.SetActive( false );

		//store world position
		_LastFPCParentTransform = _FirstPersonController.transform.parent;
		_LastFPCPosition = _FirstPersonController.transform.localPosition;
		_LastFPCRotation = _FirstPersonController.transform.localRotation;
		
		//go to ferris wheel

		_FerrisWheelLocationTransform = GameObject.Find( "FERRIS WHEEL LOCATION OBJECT" ).transform;

		_FirstPersonController.transform.parent = _FerrisWheelLocationTransform;
		_FirstPersonController.transform.localPosition = Vector3.zero;
		_FirstPersonController.transform.localRotation = Quaternion.identity;

		_FirstPersonController.GetComponent<CharacterController>().enabled = false;
		_FirstPersonController.SendMessage( "SetDisabled" );//fucking JavaScript
	}

	private void ReturnPlayerToDeparturePoint()
	{
		_ButtonLabel.text = "Ride";

		/*
		//only in night scene
		if( Application.loadedLevelName == "Nighttime" )
		{
			_SequencerButton.gameObject.SetActive( true );
		}*/

		//reset to world position
		_FirstPersonController.transform.parent = _LastFPCParentTransform;
		_FirstPersonController.transform.localPosition = _LastFPCPosition;
		_FirstPersonController.transform.localRotation = _LastFPCRotation;
		
		_FirstPersonController.GetComponent<CharacterController>().enabled = true;
		_FirstPersonController.SendMessage( "SetEnabled" );//fucking JavaScript
	}
	
	private bool 							_UserIsRidingFerrisWheel = false;
	private bool 							_IsTriggered = false;
	private Transform						_LastFPCParentTransform = null;
	private GameObject						_FirstPersonController = null;
	private Vector3							_LastFPCPosition = Vector3.zero;
	private Quaternion						_LastFPCRotation = Quaternion.identity;
	private Transform						_FerrisWheelLocationTransform = null;

	[SerializeField] private GameObject		_SequencerButton = null;
	[SerializeField] private GameObject		_EnterExitButton = null;
	[SerializeField] private UILabel		_ButtonLabel = null;
}
