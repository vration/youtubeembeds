﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof ( MeshRenderer ) )]
public class MaterialChanger : MonoBehaviour 
{
	//public Material[] Materials = new Material[0];

	public void SetMaterial( Material inMaterial )
	{
		GetComponent<MeshRenderer>().renderer.material = inMaterial;
	}

	public void SetMaterialFromString( string inMaterialName )
	{
		GetComponent<MeshRenderer>().renderer.material = Resources.Load<Material>( inMaterialName ) as Material;
	}

	public void ClearMaterial()
	{
		SetMaterialFromString( "M_Default_MainStage" );
	}
}
