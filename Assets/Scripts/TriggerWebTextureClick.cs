﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof( UWKWebView ))]
//[RequireComponent(typeof( BoxCollider ))]
public class TriggerWebTextureClick : MonoBehaviour 
{
	public bool AutoPlay = false;
	public bool PlayOnProximityTrigger = true;
	public float DelayToAutoPlay = 1;
	
	private void Awake()
	{
		if( _WebView == null )
		{
			_WebView = GetComponent<UWKWebView>();
		}
	}
	
	private void Start()
	{
		if( AutoPlay )
		{
			Invoke( "ClickWebTexture", DelayToAutoPlay );
		}
	}

	private void OnTriggerEnter()
	{
		if( PlayOnProximityTrigger )
		{
			ClickWebTexture();
		}
	}
	
	public void DelayedClickWebTexture()
	{
		_IsDisabledForDelay = true;
		Invoke( "ClickWebTexture", DelayToAutoPlay );
	}
	
	public void ClickWebTexture()
	{
		if( _IsDisabledForDelay )
		{
			_IsDisabledForDelay = false;
			UWKWebView.EnableInput();
		}
		
		Vector3 clickPosition = new Vector3( _ClickXPos, _ClickYPos, 0 );
		
		//Debug.Log ( "ClickWebTexture() " + this );
		
		if( ! UWKWebView.inputDisabled )
		{
			UWKPlugin.UWK_MsgMouseButtonDown( _WebView.ID, (int)clickPosition.x, (int)clickPosition.y, 0);
			UWKPlugin.UWK_MsgMouseButtonUp( _WebView.ID, (int)clickPosition.x, (int)clickPosition.y, 0);	
		}
	}
	
	private bool		_IsDisabledForDelay = false;
	private UWKWebView	_WebView = null;
	
	[SerializeField] int _ClickXPos = 512;
	[SerializeField] int _ClickYPos = 512;
}
