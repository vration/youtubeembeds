﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UI : MonoBehaviour 
{
	public static UI Instance = null;
	
	private void Awake()
	{
		if( Instance == null )
		{
			Instance = this;
		}
		
		if( UI2.Instance != null )
		{
			UI2.Instance = null;
		}
		
		for( int i = 0; i < _FPCPositionMarkers.Length; ++i )
		{
			Destroy( _FPCPositionMarkers[i].GetComponent<MeshRenderer>() );
			Destroy( _FPCPositionMarkers[i].GetComponent<MeshFilter>() );
		}
		
		UWKWebView.EnableInput();
	}
	
	public void MoveFPCToMarker( Transform inPositionMarker )
	{		
		//align FPC to position and rotation of position marker
		
		_FirstPersonController.localPosition = inPositionMarker.localPosition;
		
		//rotation
		Vector3 markerEulers = inPositionMarker.localRotation.eulerAngles;
		
		_FirstPersonController.localRotation = Quaternion.identity;//reset
		_FirstPersonController.Rotate( 0, markerEulers.y, 0 );
		
		_MainCamera.localRotation = Quaternion.identity;//reset
		_MainCamera.Rotate( markerEulers.x, 0, 0 );
	}
	
	//called by UI buttons
	public void ClickWebTexture( TriggerWebTextureClick inWebTextureClick )
	{
		if( _CurrentWebTexture != inWebTextureClick )
		{
			//click off playing WebTexure
			if( _CurrentWebTexture != null )
			{
				_CurrentWebTexture.ClickWebTexture();
			}
			
			//click on new WebTexture
			_CurrentWebTexture = inWebTextureClick;
			_CurrentWebTexture.ClickWebTexture();
		}
	}
	
	//called by UWKWebView mouse up
	public void RegisterWebTextureClick( TriggerWebTextureClick inWebTextureClick )
	{
		if( _CurrentWebTexture != inWebTextureClick )
		{
			//click off playing WebTexure
			if( _CurrentWebTexture != null )
			{
				_CurrentWebTexture.ClickWebTexture();
			}
			
			//click on new WebTexture
			_CurrentWebTexture = inWebTextureClick;
		}
		else
		{
			_CurrentWebTexture = null;//if already playing, deselect it
		}
	}
	
	public void LoadYouTubeScene()
	{
		_CurrentWebTexture = null;
		
		Application.LoadLevel( "YouTubeOnly" );
	}
	
	private TriggerWebTextureClick				_CurrentWebTexture = null;
	
	[SerializeField] Transform					_FirstPersonController = null;
	[SerializeField] Transform					_MainCamera = null;
	[SerializeField] Transform[]				_FPCPositionMarkers = new Transform[0];
	[SerializeField] TriggerWebTextureClick[]	_Triggers = new TriggerWebTextureClick[0];
}
