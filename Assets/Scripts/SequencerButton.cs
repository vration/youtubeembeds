﻿using UnityEngine;
using System.Collections;

public enum ButtonState
{
	Deselected,
	QueuedToPlay,
	Playing,
	FlaggedToStop
}

[RequireComponent (typeof (UISprite))]
[RequireComponent (typeof (UIButton))]
public class SequencerButton : MonoBehaviour 
{
	public int Index = 0;
	public ButtonState ButtonState = ButtonState.Deselected;

	public void Init( int inIndex, Color inDefault, Color inHover, Color inSelected, Color inSelectedHover )
	{
		_StartTime = Time.time;

		Index = inIndex;

		_Sprite = GetComponent<UISprite>();
		_Button = GetComponent<UIButton>();

		_DefaultColor = inDefault;
		_HoverColor = inHover;
		_SelectedColor = inSelected;
		_SelectedHoverColor = inSelectedHover;

		Deselect();
	}

	private void Update()
	{
		if( ButtonState == ButtonState.QueuedToPlay )
		{
			float lerpVal = ( Time.time - _StartTime ) - Mathf.Floor( Time.time - _StartTime );
			_Sprite.color = Color.Lerp( _SelectedColor, _DefaultColor, _BlinkCurve.Evaluate( lerpVal ) );
		}
	}

	public void Deselect()
	{
		ButtonState = ButtonState.Deselected;

		_Sprite.color = _DefaultColor;
		_Button.defaultColor = _DefaultColor;
		_Button.hover = _HoverColor;
		_Button.pressed = _HoverColor;
	}

	public void Select()
	{
		ButtonState = ButtonState.Playing;

		_Sprite.color = _SelectedColor;
		_Button.defaultColor = _SelectedColor;
		_Button.hover = _SelectedHoverColor;
		_Button.pressed = _SelectedHoverColor;
	}

	public void QueueToPlay()
	{
		Debug.Log( "QueueToPlay()" );
		ButtonState = ButtonState.QueuedToPlay;

		_Sprite.color = _DefaultColor;
		_Button.defaultColor = _DefaultColor;
		_Button.hover = _HoverColor;
		_Button.pressed = _HoverColor;
	}

	public void FlagToStop()
	{
		Debug.Log( "FlagToStop()" );

		ButtonState = ButtonState.FlaggedToStop;

		_Sprite.color = _SelectedColor;
		_Button.defaultColor = _SelectedColor;
		_Button.hover = _SelectedHoverColor;
		_Button.pressed = _SelectedHoverColor;
	}

	private float			_StartTime = 0;
	private UISprite 		_Sprite = null;
	private UIButton		_Button = null;

	private Color			_DefaultColor;
	private Color			_HoverColor;
	private Color			_SelectedColor;
	private Color			_SelectedHoverColor;

	[SerializeField] private AnimationCurve		_BlinkCurve = null;
}
